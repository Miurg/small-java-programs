import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static <DateAndCalendar> void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        String limits = bufferedReader.readLine();
        String[] limitsMas= limits.split("[ ]");
        int limitInt = Integer.parseInt(limitsMas[0]);

        Date[] date = new Date[limitInt];
        for (int i = 0; i < limitInt; i++)
        {
            String dateString = bufferedReader.readLine();
            try {
                date[i] = formatter.parse(dateString);
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Arrays.sort(date, 0, limitInt);

        ArrayList<Calendar> calendar = new ArrayList<>();
        for (int i = 0; i < limitInt; i++)
        {
            calendar.add(Calendar.getInstance());
            calendar.get(i).setTime(date[i]);
        }
        System.out.println(calendar.toString());

        for (int i = 0; i < limitInt-1; i++)
        {
            for (int j = i+1; j < limitInt; j++)
            {
                if (calendar.get(i).equals(calendar.get(j)))
                {
                    calendar.remove(i);
                    i--;
                    limitInt--;
                }
            }
        }

        int max = 1;
        int temp = 1;
        for (int i = 0; i < limitInt-1; i++)
        {
            calendar.get(i).add(Calendar.DAY_OF_MONTH, +1);
            if (calendar.get(i).equals(calendar.get(i+1)))
                temp++;
            else if (max<temp)
            {
                max=temp;
                temp=1;
            }
            calendar.get(i).add(Calendar.DAY_OF_MONTH, -1);
        }
        if (max<temp)
            max=temp;
        System.out.println(max);
    }
}
