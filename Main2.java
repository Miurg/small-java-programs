package com.company;

import java.sql.*;
import java.util.Scanner;

public class Main {
    // Блок объявления констант
    public static final String DB_URL = "jdbc:mysql://localhost:3306/collectioncds";

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); //Проверяем наличие JDBC драйвера для работы с БД
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/collectioncds", "root", "");//соединениесБД
            System.out.println("Соединение с СУБД выполнено.");

            Statement statement = connection.createStatement();
            Scanner in = new Scanner(System.in);
            int num = 0;
            ResultSet resultSet = null;
            while (num < 8)
            {
                System.out.println("1. SELECT * FROM performers\n" +
                        "2. SELECT * FROM performers LIMIT 4\n" +
                        "3. SELECT * FROM performers WHERE ID BETWEEN ? AND ?\n" +
                        "4. SELECT * FROM performers WHERE Country=\"?\" or Country=\"?\"\n" +
                        "5. SELECT * FROM performers ORDER BY id DESC \n" +
                        "SELECT * FROM performers ORDER BY id ASC\n" +
                        "6. SELECT age/number_songs AS age_songs FROM performers\n" +
                        "7. SELECT DISTINCT `Country` FROM performers \n" +
                        "8. Выход");
                num = in.nextInt();
                if (num == 1)
                {
                    resultSet = statement.executeQuery("SELECT * FROM performers");
                }
                else if (num == 2)
                {
                    resultSet = statement.executeQuery("SELECT * FROM performers LIMIT 4");
                }
                else if (num == 3)
                {
                    PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM performers WHERE ID BETWEEN ? AND ?");
                    System.out.println("Вывести значения от:");
                    int one = in.nextInt();
                    preparedStatement.setInt(1, one);
                    System.out.println("Вывести значения до:");
                    int two = in.nextInt();
                    preparedStatement.setInt(2, two);
                    resultSet = preparedStatement.executeQuery();
                }
                else if (num == 4)
                {
                    PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM performers WHERE Country=? or Country=?");
                    System.out.println("Вывести страну:");
                    String one = in.nextLine();
                    one = in.nextLine();
                    preparedStatement.setString(1, one);
                    System.out.println("И вывести страну:");
                    String two = in.nextLine();
                    preparedStatement.setString(2, two);
                    resultSet = preparedStatement.executeQuery();
                }
                else if (num == 5)
                {
                    System.out.println("1 - в обратном порядке, 2 - в прямом:");
                    int one = in.nextInt();
                    if (one == 1)
                    {
                        resultSet = statement.executeQuery("SELECT * FROM performers ORDER BY id DESC");
                    }
                    else if (one == 2)
                    {
                        resultSet = statement.executeQuery("SELECT * FROM performers ORDER BY id ASC");
                    }
                }
                else if (num == 6)
                {
                    resultSet = statement.executeQuery("SELECT age/number_songs AS age_songs FROM performers");
                }
                else if (num == 7)
                {
                    resultSet = statement.executeQuery("SELECT DISTINCT `Country` FROM performers");

                }
                if (num < 6)
                {
                    while(resultSet.next()){
                        int id = resultSet.getInt(1);
                        String name = resultSet.getString(2);
                        int age = resultSet.getInt(3);
                        String groupe = resultSet.getString(4);
                        String country = resultSet.getString(5);
                        int songs = resultSet.getInt(6);
                        System.out.printf("%d. %s %d %s %s %d \n", id, name, age, groupe, country, songs);
                    }
                }
                else if (num == 6)
                {
                    while(resultSet.next()){
                        int id = resultSet.getInt(1);
                        System.out.printf("%d \n", id);
                    }
                }
                else if (num == 7)
                {
                    while(resultSet.next()){
                        String id = resultSet.getString(1);
                        System.out.printf("%s \n", id);
                    }
                }
                System.out.print("\n");
            }

            connection.close();       // отключение от БД
            System.out.println("Отключение от СУБД выполнено.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace(); // обработка ошибки  Class.forName
            System.out.println("JDBC драйвер для СУБД не найден!");
        } catch (SQLException e) {
            e.printStackTrace(); // обработка ошибок  DriverManager.getConnection
            System.out.println("Ошибка SQL !");
        }
    }
}